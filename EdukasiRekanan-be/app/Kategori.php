<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $fillable = [
        'kategori'
    ];
    public function soal(){
        return $this->belongsTo(Soal::class,'idsub');
    }
}
