<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    protected $fillable = [
        'soal','idkategori','idsub','image','pil1','pil2','pil3','pil4','jawaban'
    ];
    public function soal(){
        return $this->belongsTo(Soal::class,'idsub');
    }
}
