<?php

namespace App\Http\Controllers;

use App\Soal;
use Illuminate\Support\Facades\App;
use App\Kategori;
use App\SubKategori;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\DB;

class SoalController extends Controller
{
    public $model = 'Soal';
    public $relation = [];
    public function all(Request $request){
        $select = DB::table('soals')
        ->join('kategoris', 'soals.idkategori', '=', 'kategoris.id')
        ->join('sub_kategoris', 'soals.idsub', '=', 'sub_kategoris.id')
        ->select('soals.*', 'kategoris.*','sub_kategoris.*')
        ->get();
        return Response::json($select);
    }

}
