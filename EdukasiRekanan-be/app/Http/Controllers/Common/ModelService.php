<?php
namespace App\Http\Controllers\Common;

trait ModelService{
    protected $model;
    protected $relation = [];

    public function getModels(){
        $model = '\App\\' . $this->model;

        return new $model;
    }
}
