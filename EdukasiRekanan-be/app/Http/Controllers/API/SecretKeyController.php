<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\SecretKeys;


class SecretKeyController extends BaseController
{
    public function getSecretKeys(){
        $secretkeys = SecretKeys::find(2);

        if(is_null($secretkeys)){
            return $this->sendError('Secret Keys Not Found.');
        }

        return $this->sendResponse($secretkeys->toArray(), 'Secret Keys Retrived Sucessfully.');
    }
}
