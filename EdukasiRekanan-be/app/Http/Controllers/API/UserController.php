<?php

namespace App\Http\Controllers\API;

use App\rc;
use App\Http\Controllers\Controller;
use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    protected $model = 'User';

    protected $relation = [];


    public $successStatus=200;

    public function login(Request $request){
        $credentials = [
          'email' => $request->email,
            'password' => $request->password
        ];
        if(\auth()->attempt($credentials)){
            $token = \auth()->user()->createToken('TutsForWeb')->accessToken;
//            $user = Auth::user();
//            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['token'=>$token],200);
        }else{
            return response()->json(['error'=>'Unautorised'],401);
        }
    }

    public function register(Request $request){
        $this->validate($request,[
           'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);
        $user = User::create([
           'name' => $request->name,
            'email' => $request->email,
            'nip' => $request->nip,
            'password' => bcrypt($request->password)
        ]);
        $token = $user->createToken('TutsForWeb')->accessToken;
        return response()->json(['token' => $token],200);
    }

    public function details(){
        $user = Auth::user();
        return response()->json(['success'=>$user],$this->successStatus);
    }

}
