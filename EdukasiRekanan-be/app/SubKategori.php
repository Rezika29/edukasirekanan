<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKategori extends Model
{
    protected $fillable = [
        'subkategori'
    ];
    public function soal(){
        return $this->belongsTo(Soal::class,'idsub');
    }
}
