<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//solve passport install https://stackoverflow.com/questions/58711707/composer-require-laravel-passport-not-working-in-laravel-5-8

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('soal', 'SoalController');
Route::resource('kategori', 'KategoriController');
Route::resource('subkat', 'SubKategoriController');
Route::get('all', 'SoalController@all');

